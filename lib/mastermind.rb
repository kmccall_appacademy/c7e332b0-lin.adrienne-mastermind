class Code
  attr_reader :pegs

  PEGS = {
    "B" => :blue,
    "G" => :green,
    "O" => :orange,
    "P" => :purple,
    "R" => :red,
    "Y" => :yellow
  }

  def initialize(pegs)
    @pegs = pegs
  end

  def [](idx)
    @pegs[idx]
  end

  #convert from string of letters to array of color words
  def self.parse(input)
    parsed = input.chars.map do |ch|
      raise 'Parse Error' unless PEGS.key?(ch.upcase) &&
                                 input.length == 4
      PEGS[ch.upcase]
    end

    Code.new(parsed)
  end

  def self.random
    secret_code = (0..3).map do
      PEGS.values.sample
    end
    Code.new(secret_code)
  end

  def exact_matches(other_code)
    exact_count = 0
    @pegs.each_index do |idx|
      exact_count += 1 if other_code[idx] == self[idx]
    end
    exact_count
  end

  def near_matches(other_code)
    near_count = 0
    secret_count = self.color_count
    other_count = other_code.color_count

    secret_count.each do |color, count|
      if other_count.has_key?(color)
        near_count += [count, other_count[color]].min
      end
    end

    near_count - self.exact_matches(other_code)
  end

  def ==(other_code)
    return @pegs == other_code.pegs if other_code.is_a?(Code)
    false
  end

  protected

  def color_count
    count = Hash.new(0)
    @pegs.each do |color|
      count[color] += 1
    end
    count
  end

end

class Game
  attr_reader :secret_code

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
  end

  def play
    puts 'Welcome to Mastermind!'
    puts 'Input all guesses in a four letter format.'
    puts 'Choose from B (blue), G (green), O (orange), P (purple),
          R (red), or Y (yellow).'
    puts

    count = 0
    10.times do

      code = get_guess
      count += 1
      if code == secret_code
        puts "You solved the code in #{count} turns!"
        return
      else
        display_matches(code)
        puts "#{10-count} guesses remaining."
      end
    end

    puts 'You did not solve the code after 10 turns.'
  end

  def get_guess
    begin
      print 'Guess a code: '
      guess = gets.chomp
      Code.parse(guess)
    rescue
      puts 'Please enter a valid code.'
      retry
    end
  end

def display_matches(code)
  puts "exact matches: #{@secret_code.exact_matches(code)}"
  puts "near matches: #{@secret_code.near_matches(code)}"
end
end

if __FILE__ == $PROGRAM_NAME
  Game.new.play()
end
